package com.example.gllokesh.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gllokesh.myapplication.model.DriverInfo;
import com.example.gllokesh.myapplication.model.LeaderBoard;
import com.example.gllokesh.myapplication.model.TariffInfo;
import com.example.gllokesh.myapplication.model.TransactionInfo;
import com.example.gllokesh.myapplication.utils.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LeaderBoardActivity extends AppCompatActivity {

    private Map<String, TransactionInfo> transactionInfoMap = new HashMap<String, TransactionInfo>();

    private Map<Double, ArrayList<TariffInfo>> tariffMap = new HashMap<Double, ArrayList<TariffInfo>>();

    private List<LeaderBoard> leaderBoardList = null;
    private Map<String, ArrayList<DriverInfo>> driverMap = new HashMap<String, ArrayList<DriverInfo>>();
    private RecyclerView recyclerView;
    private boolean fromDriver;
    private String driverName;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView headerTitleTv = (TextView) toolbar.findViewById(R.id.activity_title);
        headerTitleTv.setText("Leader Board");
        recyclerView = (RecyclerView) findViewById(R.id.leader_board_listview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        leaderBoardList = new ArrayList<LeaderBoard>();
        driverMap = new HashMap<String, ArrayList<DriverInfo>>();
        Utility.showProgressDialog(LeaderBoardActivity.this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("from_driver")) {
                fromDriver = bundle.getBoolean("from_driver");
            }
            if (bundle.containsKey("driver_name")) {
                driverName = bundle.getString("driver_name");
            }
        }
        loadTariffDataFromDb();
    }

    private void loadTariffDataFromDb() {
        try {
            DriverApplication.getTarrifDatabaseRef().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    tariffMap = new HashMap<Double, ArrayList<TariffInfo>>();
                    while (iterable.iterator().hasNext()) {
                        DataSnapshot dataSnap = iterable.iterator().next();
                        TariffInfo tariffInfo = (TariffInfo) dataSnap.getValue(TariffInfo.class);
                        if (tariffInfo != null) {
                            Double tariffId = Double.parseDouble(tariffInfo.getTariffId());
                            if (tariffMap.containsKey(tariffId)) {
                                ArrayList<TariffInfo> list = tariffMap.get(tariffId);
                                list.add(tariffInfo);
                                tariffMap.put(tariffId, list);
                            } else {
                                ArrayList<TariffInfo> list = new ArrayList<TariffInfo>();
                                list.add(tariffInfo);
                                tariffMap.put(tariffId, list);
                            }
                        }
                    }
                    loadTransactionDataFromDb();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(LeaderBoardActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void loadTransactionDataFromDb() {
        try {
            DriverApplication.getTransactionDatabaseRef().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    System.out.println(dataSnapshot.getValue().toString());
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    while (iterable.iterator().hasNext()) {
                        DataSnapshot dataSnap = iterable.iterator().next();
                        TransactionInfo transactionInfo = (TransactionInfo) dataSnap.getValue(TransactionInfo.class);
                        if (transactionInfo != null) {
                            transactionInfo.setTariffInfoList(tariffMap.get(Double.parseDouble(transactionInfo.getTariffId())));
                            transactionInfoMap.put(transactionInfo.getDate(), transactionInfo);
                        }
                    }
                    loadDriverTransactionInfo();
                    DriverApplication.getTransactionDatabaseRef().removeEventListener(this);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(LeaderBoardActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }

    private String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(yesterday());
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return cal.getTime();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void loadDriverTransactionInfo() {
        try {
            Query query = DriverApplication.getDriverDatabaseRef().child(getYesterdayDateString());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            Log.e("key", "==========>" + postSnapshot.getKey());
                            DriverInfo driverInfo = postSnapshot.getValue(DriverInfo.class);

                            ArrayList<DriverInfo> driverList;
                            if (driverMap.containsKey(driverInfo.getDrivername())) {
                                driverList = driverMap.get(driverInfo.getDrivername());
                                if (transactionInfoMap.containsKey(driverInfo.getDate()) && !TextUtils.isEmpty(transactionInfoMap.get(driverInfo.getDate()).getTariffId())) {
                                    driverInfo.setTariffId(transactionInfoMap.get(driverInfo.getDate()).getTariffId());
                                }
                            } else {
                                driverList = new ArrayList<DriverInfo>();
                                if (transactionInfoMap.containsKey(driverInfo.getDate()) && !TextUtils.isEmpty(transactionInfoMap.get(driverInfo.getDate()).getTariffId())) {
                                    driverInfo.setTariffId(transactionInfoMap.get(driverInfo.getDate()).getTariffId());
                                }
                            }
                            driverList.add(driverInfo);
                            driverMap.put(driverInfo.getDrivername(), driverList);
                        }

                        prepareDriverListAdapter(driverMap);
                    } else {
                        Utility.hideProgressDialog();
                        Toast.makeText(LeaderBoardActivity.this, "Information is not available. Please contact admin for detail information.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(LeaderBoardActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void prepareDriverListAdapter(Map<String, ArrayList<DriverInfo>> driverList) {
        Utility.hideProgressDialog();

        for (Map.Entry<String, ArrayList<DriverInfo>> entry : driverList.entrySet()) {
            ArrayList<DriverInfo> drivers = entry.getValue();
            double totalRide = 0.0;
            String driverName = "";
            double totalAmount = 0;
            double customerBill = 0;
            String driverCrn = "";
            Map<String, ArrayList<DriverInfo>> kmsMap = new HashMap<String, ArrayList<DriverInfo>>();

            for (DriverInfo driver : drivers) {
                totalRide = totalRide + Double.parseDouble(driver.getActual_Kms_Raw());
                driverCrn = driver.getCRN();
                driverName = driver.getDrivername();
                customerBill = customerBill + Double.parseDouble(driver.getCustomerBillRaw());
                if (kmsMap.containsKey(driver.getDate())) {
                    ArrayList<DriverInfo> infos = kmsMap.get(driver.getDate());
                    infos.add(driver);
                    kmsMap.put(driver.getDate(), infos);
                } else {
                    ArrayList<DriverInfo> infos = new ArrayList<DriverInfo>();
                    infos.add(driver);
                    kmsMap.put(driver.getDate(), infos);
                }
            }

            for (Map.Entry<String, ArrayList<DriverInfo>> e : kmsMap.entrySet()) {
                ArrayList<DriverInfo> driverInfos = e.getValue();
                String tariffId = "0";
                double totalKms = 0.0;
                for (DriverInfo driver : driverInfos) {
                    if (!TextUtils.isEmpty(driver.getTariffId())) {
                        tariffId = driver.getTariffId();
                    }
                    totalKms = totalKms + Double.parseDouble(driver.getActual_Kms_Raw());
                }
                totalAmount = totalAmount + calculateTotalAmount(totalKms, Double.parseDouble(tariffId));
            }
            LeaderBoard leaderBoard = new LeaderBoard();
            leaderBoard.setName(driverName);
            leaderBoard.setTotalAmount(totalAmount);//totalAmount);
            leaderBoard.setPendingAmount(totalAmount - customerBill);
            leaderBoard.setTotalEarning(RoundTo2Decimals(totalAmount));
            leaderBoard.setTotalKms(RoundTo2Decimals(totalRide));
            leaderBoard.setDriverCrn(driverCrn);
            leaderBoardList.add(leaderBoard);
        }
        Collections.sort(leaderBoardList, new Comparator<LeaderBoard>() {
            @Override
            public int compare(LeaderBoard o1, LeaderBoard o2) {
                return new Double(o2.getTotalKms()).compareTo(new Double(o1.getTotalKms()));
            }
        });
        recyclerView.setAdapter(new LeaderBoarderTableAdapter(LeaderBoardActivity.this, leaderBoardList, fromDriver, driverName));
    }


    private Double calculateTotalAmount(double totalKms, Double tariffId) {

        if (tariffMap.containsKey(tariffId)) {
            ArrayList<TariffInfo> infoList = tariffMap.get(tariffId);
            for (TariffInfo tariffInfo : infoList) {
                double min = Double.parseDouble(tariffInfo.getMinKms());
                double max = Double.parseDouble(tariffInfo.getMaxKms());
                if (totalKms >= min && totalKms <= max) {
                    return Double.parseDouble(tariffInfo.getAmount());
                }
            }
        }
        return 0.0;
    }


    private double RoundTo2Decimals(double val) {
        DecimalFormat df2 = new DecimalFormat("###.##");
        return Double.valueOf(df2.format(val));
    }


}
