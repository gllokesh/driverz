package com.example.gllokesh.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gllokesh.myapplication.model.KeyInfo;
import com.example.gllokesh.myapplication.model.RolesInfo;
import com.example.gllokesh.myapplication.model.SpreadsheetKeys;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText userNameEdit;
    private SpreadsheetKeys mSpreadsheetKeys;
    private List<RolesInfo> rolesInfos;
    private EditText passwordEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView headerTitleTv = (TextView) toolbar.findViewById(R.id.activity_title);
        headerTitleTv.setText("DriverZ");

        Button siginBtn = (Button) findViewById(R.id.sign_btn);
        userNameEdit = (EditText) findViewById(R.id.user_name_edit);
        passwordEdit = (EditText) findViewById(R.id.password_edit);
        siginBtn.setOnClickListener(this);
        downloadGoogleSheetKeys();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_btn:
                if (TextUtils.isEmpty(userNameEdit.getText()) || TextUtils.isEmpty(passwordEdit.getText())) {
                    Toast.makeText(this, "User ID and password are mandatory.", Toast.LENGTH_SHORT).show();
                } else {
                    loginUser(userNameEdit.getText().toString(), passwordEdit.getText().toString());
                }
                //startActivity(new Intent(MainActivity.this, AdminActivity.class));
                break;
            case R.id.update_database_btn:
                break;
        }

    }

    private void loginUser(String userName, final String password) {

        Query query = DriverApplication.getRoleDatabaseRef().child(userName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.e("Query key", dataSnapshot.getKey());
                    Log.e("Query value", dataSnapshot.getValue().toString());
                    RolesInfo rolesInfo = dataSnapshot.getValue(RolesInfo.class);
                    Log.e("Query", "dataSnapshot.exists()");
                    if (password.equals(rolesInfo.getPassword())) {
                        if ("Admin".equalsIgnoreCase(rolesInfo.getRole())) {
                            startActivity(new Intent(MainActivity.this, AdminActivity.class));
                            finish();
                        } else {
                            Intent intent = new Intent(MainActivity.this, DriverActivity.class);
                            intent.putExtra("name", rolesInfo.getName());
                            intent.putExtra("id", rolesInfo.getId());
                            startActivity(intent);
                            finish();
                        }
                        Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "User is not present in Database", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(MainActivity.this, "Something went wrong please try again later!", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void downloadGoogleSheetKeys() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseGoogleSheetKeys(object);
                downloadRolesInfoFromGoogleSheet();
            }
        }).execute("https://spreadsheets.google.com/tq?key=14ENSdLcqk5s0BmlKdKSLAEI3g9yIaTXatML2OnIVCAg&gid=1021655831");
    }

    private void downloadRolesInfoFromGoogleSheet() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseAndUpdatedRolesInfo(object);


            }
        }).execute("https://spreadsheets.google.com/tq?key=" + mSpreadsheetKeys.getRoleSheetInfo().getKey());
    }

    private void parseAndUpdatedRolesInfo(JSONObject jsonObject) {
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            rolesInfos = new ArrayList<RolesInfo>();
            for (int r = 0; r < rows.length(); ++r) {
                RolesInfo rolesInfo = new RolesInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                rolesInfo.setName(columns.getJSONObject(0).getString("v"));
                if (columns.getJSONObject(1) != null) {
                    rolesInfo.setRole(columns.getJSONObject(1).getString("v"));
                }
                if (columns.length() > 2 && columns.getJSONObject(2) != null) {
                    rolesInfo.setId(columns.getJSONObject(2).getString("v"));
                }
                if (columns.length() > 3 && columns.getJSONObject(3) != null) {
                    rolesInfo.setPassword(columns.getJSONObject(3).getString("v"));
                }
                rolesInfos.add(rolesInfo);
                DriverApplication.getRoleDatabaseRef().child(rolesInfo.getId()).setValue(rolesInfo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*private void downloadVehicleInfoDataFromGoogleSheet(String googleSheetKey, boolean isUpdate) {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
            }
        }).execute("https://spreadsheets.google.com/tq?key=" + googleSheetKey);
    }

    private void downloadDriverInfoFromGoogleSheet(String googleSheetKey, boolean isUpdate) {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
            }
        }).execute("https://spreadsheets.google.com/tq?key=" + googleSheetKey + "&gid=652948748");

    }

    private void downloadDriverBonusInfoFromGoogleSheet(String googleSheetKey, boolean isUpdate) {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
            }
        }).execute("https://spreadsheets.google.com/tq?key=" + googleSheetKey + "&&ggid=2000198270");
    }


    private void updateOrCreatedDriverInfoDatabase(JSONObject jsonObject, boolean isUpdate) {
        Log.e("JSON", jsonObject.toString());
        List<DriverInfo> driverInfos = new ArrayList<DriverInfo>();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            Log.e("Driver rows", rows.toString());

            for (int r = 0; r < rows.length(); ++r) {
                Log.e("Driver r", r + " ===");

                DriverInfo driverInfo = new DriverInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                driverInfo.setDate(columns.getJSONObject(0).getString("v"));
                driverInfo.setCarnumber(columns.getJSONObject(1).getString("v"));
                driverInfo.setDriverID(columns.getJSONObject(2).getString("v"));
                driverInfo.setDrivername(columns.getJSONObject(3).getString("v"));
                driverInfo.setDrivernumber(columns.getJSONObject(4).getString("v"));
                driverInfo.setCRN(columns.getJSONObject(5).getString("v"));
                driverInfo.setCompletionStatus(columns.getJSONObject(6).getString("v"));
                driverInfo.setCustomerBillRaw(columns.getJSONObject(7).getString("v"));
                driverInfo.setPaid_by_ola_money_Raw(columns.getJSONObject(8).getString("v"));
                driverInfo.setOperatorBillRaw(columns.getJSONObject(9).getString("v"));
                driverInfo.setPeakPricingRaw(columns.getJSONObject(10).getString("v"));
                driverInfo.setRideEarningsRaw(columns.getJSONObject(11).getString("v"));

                driverInfo.setTDSRaw(columns.getJSONObject(12).getString("v"));
                driverInfo.setToll_ParkingRaw(columns.getJSONObject(13).getString("v"));
                driverInfo.setCash_collected_by_driver_Raw(columns.getJSONObject(14).getString("v"));
                driverInfo.setOla_to_Pay(columns.getJSONObject(15).getString("v"));
                driverInfo.setCategory(columns.getJSONObject(16).getString("v"));
                driverInfo.setPick_up_time(columns.getJSONObject(17).getString("v"));
                driverInfo.setActual_Kms_Raw(columns.getJSONObject(18).getString("v"));
                driverInfo.setTrip_Time_Raw(columns.getJSONObject(19).getString("v"));
                driverInfo.setFare_Raw(columns.getJSONObject(20).getString("v"));
                driverInfo.setShare_OSNs(columns.getJSONObject(21).getString("v"));
                driverInfo.setNumber_of_share_OSNs(columns.getJSONObject(22).getString("v"));
                driverInfo.setBookings_Completed_Raw(columns.getJSONObject(23).getString("v"));
                driverInfos.add(driverInfo);
                //myRef1.child("vehicles").child(driverInfo.getCarnumber()).setValue(driverInfos);

            }
//            myRef1.child("drivers").setValue(driverInfos);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void updateOrCreateVehicleInfoDatabase(JSONObject jsonObject, boolean isUpdate) {
        Log.e("JSON", jsonObject.toString());
        List<VehicleInfo> vehicleInfos = new ArrayList<VehicleInfo>();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            Log.e("rows", rows.toString());

            for (int r = 0; r < rows.length(); ++r) {
                VehicleInfo vehicleInfo = new VehicleInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                vehicleInfo.setVehicleNumber(columns.getJSONObject(0).getString("v"));
                vehicleInfo.setVehicleType(columns.getJSONObject(1).getString("v"));
                vehicleInfo.setVehicleName(columns.getJSONObject(2).getString("v"));
                vehicleInfo.setVehicleOwner(columns.getJSONObject(3).getString("v"));
                vehicleInfo.setVehicleOwnerNumber(columns.getJSONObject(4).getString("v"));
                vehicleInfo.setVehicleOwnerAddress(columns.getJSONObject(5).getString("v"));
                vehicleInfo.setVehicleOwnerShare(columns.getJSONObject(6).getString("v"));
                vehicleInfo.setVehicleServiceShare(columns.getJSONObject(7).getString("v"));
                vehicleInfo.setVehicleDriverShare(columns.getJSONObject(8).getString("v"));
                Log.e("vehicle", vehicleInfo.toString());
                vehicleInfos.add(vehicleInfo);
            }

//            myRef.child("vehicles").child("rajesh").setValue(vehicleInfos);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateOrCreateBonusInfoDatabase(JSONObject jsonObject, boolean isUpdate) {
        Log.e("JSON", jsonObject.toString());
        List<BonusInfo> bonusInfos = new ArrayList<BonusInfo>();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            Log.e("Driver rows", rows.toString());

            for (int r = 0; r < rows.length(); ++r) {
                Log.e("Driver r", r + " ===");
                BonusInfo bonusInfo = new BonusInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                bonusInfo.setKms(columns.getJSONObject(0).getString("v"));
                bonusInfo.setAmount(columns.getJSONObject(1).getString("v"));
                bonusInfos.add(bonusInfo);

            }
//            bonusOptionsDb.child("bonus").setValue(bonusInfos);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/


    private void parseGoogleSheetKeys(JSONObject jsonObject) {
        Log.e("JSON", jsonObject.toString());
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");

            JSONObject rolesJson = rows.getJSONObject(1);
            KeyInfo rolesKeyInfo = new KeyInfo();
            JSONArray rolesColumns = rolesJson.getJSONArray("c");
            rolesKeyInfo.setDocumentName(rolesColumns.getJSONObject(0).getString("v"));
            rolesKeyInfo.setKey(rolesColumns.getJSONObject(1).getString("v"));
            rolesKeyInfo.setDate(rolesColumns.getJSONObject(2).getString("v"));


            JSONObject vehicleJson = rows.getJSONObject(2);
            KeyInfo vehicleKeyInfo = new KeyInfo();
            JSONArray vehicleColumns = vehicleJson.getJSONArray("c");
            vehicleKeyInfo.setDocumentName(vehicleColumns.getJSONObject(0).getString("v"));
            vehicleKeyInfo.setKey(vehicleColumns.getJSONObject(1).getString("v"));
            vehicleKeyInfo.setDate(vehicleColumns.getJSONObject(2).getString("v"));


            JSONObject tariffJson = rows.getJSONObject(3);
            KeyInfo tariffKeyInfo = new KeyInfo();
            JSONArray tariffColumns = tariffJson.getJSONArray("c");
            tariffKeyInfo.setDocumentName(tariffColumns.getJSONObject(0).getString("v"));
            tariffKeyInfo.setKey(tariffColumns.getJSONObject(1).getString("v"));
            tariffKeyInfo.setDate(tariffColumns.getJSONObject(2).getString("v"));


            mSpreadsheetKeys = new SpreadsheetKeys();
            mSpreadsheetKeys.setRoleSheetInfo(rolesKeyInfo);
            mSpreadsheetKeys.setTariffSheetInfo(tariffKeyInfo);
            mSpreadsheetKeys.setVehicleSheetInfo(vehicleKeyInfo);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
