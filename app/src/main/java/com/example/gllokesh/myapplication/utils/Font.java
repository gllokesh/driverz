package com.example.gllokesh.myapplication.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public enum Font {
    EMPRINT_BOLD(1, "EMprint-Bold.ttf"),
    EMPRINT_BOLD_ITALIC(2, "EMprint-BoldItalic.ttf"),
    EMPRINT_LIGHT(3, "EMprint-Light.ttf"),
    EMPRINT_LIGHT_ITALIC(4, "EMprint-LightItalic.ttf"),
    EMPRINT_REGULAR(5, "EMprint-Regular.ttf"),
    EMPRINT_REGULAR_ITALIC(6, "EMprint-RegularItalic.ttf"),
    EMPRINT_SEMI_BOLD(7, "EMprint-Semibold.ttf"),
    EMPRINT_SEMI_BOLD_ITALIC(8, "EMprint-SemiboldItalic.ttf");


    private static Map<Integer, Font> valueLookup;
    private int value;
    private String filename;

    Font(int value, String filename) {
        this.value = value;
        this.filename = filename;
    }

    public static Typeface getTypefaceByValue(final Context context, final int value) {
        if (valueLookup == null) {
            valueLookup = new HashMap<>();
            for (Font font : Font.values()) {
                valueLookup.put(font.getValue(), font);
            }
        }

        final Font font = valueLookup.get(value);
        return font != null ? font.getTypeface(context) : null;
    }

    public int getValue() {
        return value;
    }

    public Typeface getTypeface(final Context context) {
        return Typeface.createFromAsset(context.getAssets(), filename);//String.format("fonts/%s", filename));
    }
}