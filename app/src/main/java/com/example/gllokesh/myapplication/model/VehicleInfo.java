package com.example.gllokesh.myapplication.model;

/**
 * Created by photonuser on 8/4/17.
 */

public class VehicleInfo {
    private String vehicleNumber;
    private String vehicleType;
    private String vehicleName;
    private String vehicleOwner;
    private String vehicleOwnerNumber;
    private String vehicleOwnerAddress;
    private String vehicleOwnerShare;
    private String vehicleServiceShare;
    private String vehicleDriverShare;

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleOwner() {
        return vehicleOwner;
    }

    public void setVehicleOwner(String vehicleOwner) {
        this.vehicleOwner = vehicleOwner;
    }

    public String getVehicleOwnerNumber() {
        return vehicleOwnerNumber;
    }

    public void setVehicleOwnerNumber(String vehicleOwnerNumber) {
        this.vehicleOwnerNumber = vehicleOwnerNumber;
    }

    public String getVehicleOwnerAddress() {
        return vehicleOwnerAddress;
    }

    public void setVehicleOwnerAddress(String vehicleOwnerAddress) {
        this.vehicleOwnerAddress = vehicleOwnerAddress;
    }

    public String getVehicleOwnerShare() {
        return vehicleOwnerShare;
    }

    public void setVehicleOwnerShare(String vehicleOwnerShare) {
        this.vehicleOwnerShare = vehicleOwnerShare;
    }

    public String getVehicleServiceShare() {
        return vehicleServiceShare;
    }

    public void setVehicleServiceShare(String vehicleServiceShare) {
        this.vehicleServiceShare = vehicleServiceShare;
    }

    public String getVehicleDriverShare() {
        return vehicleDriverShare;
    }

    public void setVehicleDriverShare(String vehicleDriverShare) {
        this.vehicleDriverShare = vehicleDriverShare;
    }

    @Override
    public String toString() {
        return this.getVehicleName() +" " + vehicleNumber +" "+
        vehicleType +" " +
        vehicleName +" " +
        vehicleOwner +"  "+
        vehicleOwnerNumber +" "+
        vehicleOwnerAddress + "  "+
        vehicleOwnerShare+"  "+
        vehicleServiceShare+"  "+
        vehicleDriverShare+"  ";
    }
}
