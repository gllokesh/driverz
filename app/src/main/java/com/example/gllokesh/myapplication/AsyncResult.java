package com.example.gllokesh.myapplication;

import org.json.JSONObject;

interface AsyncResult {
    void onResult(JSONObject object);
}