package com.example.gllokesh.myapplication.model;

/**
 * Created by gllokesh on 9/25/17.
 */

public class LeaderBoard {
    private String name;
    private int totalRides;
    private double totalEarning;
    private double totalKms;

    private double totalAmount;
    private double pendingAmount;
    private String driverCrn;


    public String getDriverCrn() {
        return driverCrn;
    }

    public void setDriverCrn(String driverCrn) {
        this.driverCrn = driverCrn;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(double pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public double getTotalKms() {
        return totalKms;
    }

    public void setTotalKms(double totalKms) {
        this.totalKms = totalKms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalRides() {
        return totalRides;
    }

    public void setTotalRides(int totalRides) {
        this.totalRides = totalRides;
    }

    public double getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(double totalEarning) {
        this.totalEarning = totalEarning;
    }
}
