package com.example.gllokesh.myapplication;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.gllokesh.myapplication.utils.FirebaseUtils;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by gllokesh on 8/30/17.
 */

public class DriverApplication extends Application {


    private static DatabaseReference roleDatabaseRef;
    private static DatabaseReference VehicleDatabaseRef;
    private static DatabaseReference tarrifDatabaseRef;
    private static DatabaseReference transactionDatabaseRef;
    private static DatabaseReference driverDatabaseRef;
    private static DatabaseReference paymentDatabaseRef;
    private static Application application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static DatabaseReference getRoleDatabaseRef() {
        if(roleDatabaseRef == null) {
            roleDatabaseRef = FirebaseUtils.getRolesDatabaseReference(application);
        }
        return roleDatabaseRef;
    }

    public static DatabaseReference getTransactionDatabaseRef() {
        if(transactionDatabaseRef == null) {
            transactionDatabaseRef = FirebaseUtils.getTransactionDatabaseReference(application);
        }
        return transactionDatabaseRef;
    }

    public static DatabaseReference getVehicleDatabaseRef() {
        if(VehicleDatabaseRef == null) {
            VehicleDatabaseRef = FirebaseUtils.getVehicleRegdDatabaseReference(application);
        }
        return VehicleDatabaseRef;
    }

    public static DatabaseReference getTarrifDatabaseRef() {
        if(tarrifDatabaseRef == null) {
            tarrifDatabaseRef = FirebaseUtils.getTariffDatabaseReference(application);
        }
        return tarrifDatabaseRef;
    }


    public static DatabaseReference getDriverDatabaseRef() {
        if(driverDatabaseRef == null) {
            driverDatabaseRef = FirebaseUtils.getRawCrnsDatabaseReference(application);
        }
        return driverDatabaseRef;
    }

    public static DatabaseReference getPaymentDatabaseRef() {
        if(paymentDatabaseRef == null) {
            paymentDatabaseRef = FirebaseUtils.getPaymentsDatabaseReference(application);
        }
        return paymentDatabaseRef;
    }

}
