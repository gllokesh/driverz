package com.example.gllokesh.myapplication.model;

/**
 * Created by gllokesh on 9/24/17.
 */

public class TariffInfo {
    private String minKms;
    private String maxKms;
    private String amount;
    private String tariffId;
    private String roles;

    public String getMinKms() {
        return minKms;
    }

    public void setMinKms(String minKms) {
        this.minKms = minKms;
    }

    public String getMaxKms() {
        return maxKms;
    }

    public void setMaxKms(String maxKms) {
        this.maxKms = maxKms;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
