package com.example.gllokesh.myapplication.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by lokesh_n on 5/31/2016.
 */
public class Utility {
    public static final String MANAGER = "Parking_manager";
    public static final String CLIENT = "Parking_client";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static ProgressDialog mProgressDialog;
    private static String printerName;
    private static boolean mIsPrinterConnected;
    private static Handler mConnectionHandler;
    private static Handler handler;

    public static void showProgressDialog(Activity context) {
        if (!context.isFinishing()) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }


    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    public static String getString(JSONArray jsonArray, int index) {
        try {
            if (jsonArray.length() > index && jsonArray.getJSONObject(index) != null) {
                return jsonArray.getJSONObject(index).optString("v", "");
            }
        } catch (JSONException e) {
            return "";
        }
        return "";
    }
}
