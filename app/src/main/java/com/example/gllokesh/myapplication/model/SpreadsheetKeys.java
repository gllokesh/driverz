package com.example.gllokesh.myapplication.model;

/**
 * Created by gllokesh on 8/29/17.
 */

public class SpreadsheetKeys {
    private KeyInfo roleSheetInfo;
    private KeyInfo vehicleSheetInfo;
    private KeyInfo tariffSheetInfo;

    public KeyInfo getRoleSheetInfo() {
        return roleSheetInfo;
    }

    public void setRoleSheetInfo(KeyInfo roleSheetInfo) {
        this.roleSheetInfo = roleSheetInfo;
    }

    public KeyInfo getVehicleSheetInfo() {
        return vehicleSheetInfo;
    }

    public void setVehicleSheetInfo(KeyInfo vehicleSheetInfo) {
        this.vehicleSheetInfo = vehicleSheetInfo;
    }

    public KeyInfo getTariffSheetInfo() {
        return tariffSheetInfo;
    }

    public void setTariffSheetInfo(KeyInfo tariffSheetInfo) {
        this.tariffSheetInfo = tariffSheetInfo;
    }
}
