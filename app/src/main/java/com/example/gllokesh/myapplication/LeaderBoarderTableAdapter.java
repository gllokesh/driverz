package com.example.gllokesh.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gllokesh.myapplication.model.LeaderBoard;

import java.util.List;


/**
 * Created by photonuser on 7/22/17.
 */

                public class LeaderBoarderTableAdapter extends RecyclerView.Adapter<LeaderBoarderTableAdapter.MyViewHolder> {


    private final Context context;
    private final List<LeaderBoard> leaderBoards;
    private final boolean fromDriver;
    private final String driverName;

    public LeaderBoarderTableAdapter(Context context, List<LeaderBoard> leaderBoards, boolean fromDriver, String driverName) {
        this.context = context;
        this.leaderBoards = leaderBoards;
        this.fromDriver = fromDriver;
        this.driverName = driverName;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leader_board_table_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.driverNameTv.setText(leaderBoards.get(position).getName());
        holder.totalKms.setText(leaderBoards.get(position).getTotalKms()+"");
        holder.totalAmountTv.setText(""+leaderBoards.get(position).getTotalEarning());
       // holder.pendingAmountTv.setText("Pending: " + "Rs. " + leaderBoards.get(position).getPendingAmount());
        if(fromDriver) {
            if(driverName.equalsIgnoreCase(leaderBoards.get(position).getName())) {
                holder.totalAmountTv.setVisibility(View.VISIBLE);
            } else {
                holder.totalAmountTv.setVisibility(View.VISIBLE);
                holder.totalAmountTv.setText("---");
            }
            //holder.pendingAmountTv.setVisibility(View.INVISIBLE);
        } else {
            //holder.pendingAmountTv.setVisibility(View.VISIBLE);
        }
       // holder.driverCRN.setText("CRN: " + leaderBoards.get(position).getDriverCrn());
        Log.e("====" , "Pending amount ===== > " + leaderBoards.get(position).getPendingAmount());
        Log.e("====" , " Total amount ===== > " + leaderBoards.get(position).getTotalAmount());
    }

    @Override
    public int getItemCount() {
        return leaderBoards.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView driverNameTv;
        public TextView totalKms;
        public TextView totalAmountTv;

        public MyViewHolder(View view) {
            super(view);
            driverNameTv = (TextView) view.findViewById(R.id.leader_board_driver_name_tv);
            totalKms = (TextView) view.findViewById(R.id.leader_board_km_tv);
            totalAmountTv = (TextView) view.findViewById(R.id.leader_board_earning_tv);
        }
    }
}
