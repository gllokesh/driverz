package com.example.gllokesh.myapplication.model;

/**
 * Created by photonuser on 8/5/17.
 */

public class BonusInfo {
    private String kms;
    private String amount;

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
