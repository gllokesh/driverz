package com.example.gllokesh.myapplication.model;

/**
 * Created by gllokesh on 10/28/17.
 */

public class DriverPaymentInfo {
    private String netEarnings;
    private String cashCollected;
    private String toll;
    private String paymentByAkv="0";
    private String pendingAmount;

    public String getNetEarnings() {
        return netEarnings;
    }

    public void setNetEarnings(String netEarnings) {
        this.netEarnings = netEarnings;
    }

    public String getCashCollected() {
        return cashCollected;
    }

    public void setCashCollected(String cashCollected) {
        this.cashCollected = cashCollected;
    }

    public String getToll() {
        return toll;
    }

    public void setToll(String toll) {
        this.toll = toll;
    }

    public String getPaymentByAkv() {
        return paymentByAkv;
    }

    public void setPaymentByAkv(String paymentByAkv) {
        this.paymentByAkv = paymentByAkv;
    }

    public String getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(String pendingAmount) {
        this.pendingAmount = pendingAmount;
    }
}
