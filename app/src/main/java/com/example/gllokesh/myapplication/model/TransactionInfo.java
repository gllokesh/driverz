package com.example.gllokesh.myapplication.model;

import java.util.List;

/**
 * Created by ga-mlsdiscovery on 9/1/17.
 */

public class TransactionInfo {

    private String date;
    private String rawCmsCode;
    private String tariffId;
    private List<TariffInfo> tariffInfoList;

    public List<TariffInfo> getTariffInfoList() {
        return tariffInfoList;
    }

    public void setTariffInfoList(List<TariffInfo> tariffInfoList) {
        this.tariffInfoList = tariffInfoList;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRawCmsCode() {
        return rawCmsCode;
    }

    public void setRawCmsCode(String rawCmsCode) {
        this.rawCmsCode = rawCmsCode;
    }
}
