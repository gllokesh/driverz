package com.example.gllokesh.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gllokesh.myapplication.model.DriverInfo;
import com.example.gllokesh.myapplication.model.PaymentInfo;
import com.example.gllokesh.myapplication.model.TariffInfo;
import com.example.gllokesh.myapplication.model.TransactionInfo;
import com.example.gllokesh.myapplication.utils.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DriverActivity extends AppCompatActivity implements View.OnClickListener {


    private List<String> listOfDates;
    private Spinner spinnerDropDown;
    private TextView netErningTv;
    private TextView cashCollectedTv;
    private TextView tollPaidTv;
    private TextView paymentAvkTv;
    private TextView pendingBalanceTv;
    private TextView driverNameTv;
    private TextView selectedDateTv;
    private String driverName;
    private String selectedDate;
    private LinearLayout paymentInfoLayout;
    private TextView informationTv;

    private Map<String, TransactionInfo> transactionInfoMap;

    private Map<Double, ArrayList<TariffInfo>> tariffMap;
    private ImageView logOutBtn;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView headerTitleTv = (TextView) toolbar.findViewById(R.id.activity_title);
        headerTitleTv.setText("Driver Screen");
        netErningTv = (TextView) findViewById(R.id.net_earning_tv);
        cashCollectedTv = (TextView) findViewById(R.id.cash_collected_tv);
        tollPaidTv = (TextView) findViewById(R.id.toll_paid_tv);
        paymentAvkTv = (TextView) findViewById(R.id.payment_by_akv_tv);
        pendingBalanceTv = (TextView) findViewById(R.id.closing_balance_tv);
        driverNameTv = (TextView) findViewById(R.id.driver_name_tv);
        selectedDateTv = (TextView) findViewById(R.id.selected_date_tv);
        paymentInfoLayout = (LinearLayout) findViewById(R.id.scrollView_layout);
        RelativeLayout leaderLayout = (RelativeLayout) findViewById(R.id.leader_board_layout);
        Button leaderBoardBtn = (Button) findViewById(R.id.leader_board_btn);
        informationTv = (TextView) findViewById(R.id.information_tv);
        logOutBtn = (ImageView)findViewById(R.id.logout_btn);
        logOutBtn.setVisibility(View.VISIBLE);
        logOutBtn.setOnClickListener(this);
        TextView viewDriverInfoTv = (TextView) findViewById(R.id.driver_complete_info);
        SpannableString content = new SpannableString("My Complete Transactions.");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        viewDriverInfoTv.setText(content);
        transactionInfoMap = new HashMap<String, TransactionInfo>();
        tariffMap = new HashMap<Double, ArrayList<TariffInfo>>();

        viewDriverInfoTv.setOnClickListener(this);
        leaderBoardBtn.setOnClickListener(this);
        leaderLayout.setOnClickListener(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("name")) {
            driverName = bundle.getString("name");
            driverNameTv.setText(driverName + "'s Account");
        }

        spinnerDropDown = (Spinner) findViewById(R.id.spinner1);
        listOfDates = new ArrayList<String>();
        transactionInfoMap = new HashMap<String, TransactionInfo>();
        loadTariffDataFromDb();
        spinnerDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int sid = spinnerDropDown.getSelectedItemPosition();
                String date = listOfDates.get(sid);
                if (!date.equalsIgnoreCase("Select Date")) {
                    selectedDate = date;
                    selectedDateTv.setText(selectedDate);
                    test(selectedDate, driverName);
                    Toast.makeText(getBaseContext(), "You have selected Date : " + listOfDates.get(sid), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void test(final String date, String name) {
        Utility.showProgressDialog(DriverActivity.this);
        final double[] cashCollected = {0.0};
        final double[] toll = {0.0};
        final double[] netEarning = {0.0};
        Query query = DriverApplication.getDriverDatabaseRef().child(date);
        Query q = query.orderByChild("drivername").startAt(name).endAt(name);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    double totalKms = 0;
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        DriverInfo driverInfo = postSnapshot.getValue(DriverInfo.class);
                        cashCollected[0] += Double.parseDouble(driverInfo.getCash_collected_by_driver_Raw());
                        Log.e("cash collected", "  *********** " + driverInfo.getCash_collected_by_driver_Raw());

                        toll[0] += Double.parseDouble(driverInfo.getToll_ParkingRaw());
                        Log.e("total kms", "  == " + driverInfo.getActual_Kms_Raw());
                        totalKms = totalKms + Double.parseDouble(driverInfo.getActual_Kms_Raw());
                        netEarning[0] += Double.parseDouble(driverInfo.getCustomerBillRaw()) + Double.parseDouble(driverInfo.getPaid_by_ola_money_Raw()) + Double.parseDouble(driverInfo.getToll_ParkingRaw());
                    }
                    Log.e("total kms", "  ==>>>> " + totalKms);

                    cashCollectedTv.setText("Rs." + String.valueOf(Math.abs(cashCollected[0])));
                    tollPaidTv.setText("Rs." + String.valueOf(toll[0]));
                    TransactionInfo transactionInfo = transactionInfoMap.get(date);
                    double totalAmount = calculateTotalAmount(totalKms, Double.parseDouble(transactionInfo.getTariffId()));
                    netErningTv.setText("Rs." + RoundTo2Decimals(totalAmount));

                    loadDriverPaymentInfo(selectedDate, totalAmount, cashCollected[0]);
                } else {
                    Utility.hideProgressDialog();
                    paymentInfoLayout.setVisibility(View.INVISIBLE);
                    informationTv.setVisibility(View.VISIBLE);
                    Toast.makeText(DriverActivity.this, "Information is not available. Please contact admin for detail information.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utility.hideProgressDialog();
            }
        });
    }

    private double RoundTo2Decimals(double val) {
        DecimalFormat df2 = new DecimalFormat("###.##");
        return Double.valueOf(df2.format(val));
    }

    private void loadDriverPaymentInfo(String selectedDate, final double amount, final double collectedCash) {
        Query query = DriverApplication.getPaymentDatabaseRef().child(selectedDate);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Utility.hideProgressDialog();
                if (dataSnapshot.exists()) {
                    informationTv.setVisibility(View.GONE);
                    paymentInfoLayout.setVisibility(View.VISIBLE);
                    GenericTypeIndicator<ArrayList<PaymentInfo>> t = new GenericTypeIndicator<ArrayList<PaymentInfo>>() {
                    };
                    ArrayList<PaymentInfo> paymentInfos = dataSnapshot.getValue(t);
                    double paidAmount = 0;
                    for (PaymentInfo paymentInfo : paymentInfos) {
                        if (driverName.equalsIgnoreCase(paymentInfo.getDriverName())) {
                            paidAmount = paidAmount + Double.parseDouble(paymentInfo.getAmount());
                        }
                    }
                    paymentAvkTv.setText(paidAmount + "");

                    pendingBalanceTv.setText("Rs." + String.valueOf(amount - paidAmount - Math.abs(collectedCash)));
                } else {
                    paymentInfoLayout.setVisibility(View.INVISIBLE);
                    informationTv.setVisibility(View.VISIBLE);
                    Toast.makeText(DriverActivity.this, "Information is not available. Please contact admin for detail information.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utility.hideProgressDialog();

            }
        });
    }

    private void loadTariffDataFromDb() {
        try {
            Utility.showProgressDialog(DriverActivity.this);
            DriverApplication.getTarrifDatabaseRef().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    tariffMap = new HashMap<Double, ArrayList<TariffInfo>>();
                    while (iterable.iterator().hasNext()) {
                        DataSnapshot dataSnap = iterable.iterator().next();
                        TariffInfo tariffInfo = (TariffInfo) dataSnap.getValue(TariffInfo.class);
                        if (tariffInfo != null) {
                            Double tariffId = Double.parseDouble(tariffInfo.getTariffId());
                            if (tariffMap.containsKey(tariffId)) {
                                ArrayList<TariffInfo> list = tariffMap.get(tariffId);
                                list.add(tariffInfo);
                                tariffMap.put(tariffId, list);
                            } else {
                                ArrayList<TariffInfo> list = new ArrayList<TariffInfo>();
                                list.add(tariffInfo);
                                tariffMap.put(tariffId, list);
                            }
                        }
                    }
                    loadTransactionDataFromDb();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(DriverActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void loadTransactionDataFromDb() {
        try {
            DriverApplication.getTransactionDatabaseRef().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Utility.hideProgressDialog();
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    while (iterable.iterator().hasNext()) {
                        DataSnapshot dataSnap = iterable.iterator().next();
                        TransactionInfo transactionInfo = (TransactionInfo) dataSnap.getValue(TransactionInfo.class);
                        if (transactionInfo != null) {
                            transactionInfo.setTariffInfoList(tariffMap.get(Double.parseDouble(transactionInfo.getTariffId())));
                            transactionInfoMap.put(transactionInfo.getDate(), transactionInfo);

                            listOfDates.add(transactionInfo.getDate());
                            transactionInfoMap.put(transactionInfo.getDate(), transactionInfo);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(DriverActivity.this, android.
                                    R.layout.simple_spinner_dropdown_item, listOfDates);
                            spinnerDropDown.setAdapter(adapter);

                        }

                    }
//                    test(selectedDate, driverName);
//                    loadDriverTransactionInfo();
                    DriverApplication.getTransactionDatabaseRef().removeEventListener(this);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(DriverActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private Double calculateTotalAmount(double totalKms, Double tariffId) {

        if (tariffMap.containsKey(tariffId)) {
            ArrayList<TariffInfo> infoList = tariffMap.get(tariffId);
            for (TariffInfo tariffInfo : infoList) {
                double min = Double.parseDouble(tariffInfo.getMinKms());
                double max = Double.parseDouble(tariffInfo.getMaxKms());
                if (totalKms >= min && totalKms <= max) {
                    return Double.parseDouble(tariffInfo.getAmount());
                }
            }
        }
        return 0.0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.leader_board_layout:
                Intent intent = new Intent(this, LeaderBoardActivity.class);
                intent.putExtra("driver_name", driverName);
                intent.putExtra("from_driver", true);
                startActivity(intent);
                break;
            case R.id.leader_board_btn:
                Intent intent1 = new Intent(this, LeaderBoardActivity.class);
                intent1.putExtra("driver_name", driverName);
                intent1.putExtra("from_driver", true);
                startActivity(intent1);
                break;
            case R.id.driver_complete_info:
                Intent driverInfo = new Intent(this, DriverInfoActivity.class);
                driverInfo.putExtra("driver_name", driverName);
                driverInfo.putExtra("from_driver", true);
                startActivity(driverInfo);
                break;
            case R.id.logout_btn:
                Intent logoutIntent = new Intent(this, MainActivity.class);
                startActivity(logoutIntent);
                finish();
                break;
        }
    }
}
