package com.example.gllokesh.myapplication;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gllokesh.myapplication.model.DriverPaymentInfo;
import com.example.gllokesh.myapplication.model.LeaderBoard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by gllokesh on 10/28/17.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private Map<String, DriverPaymentInfo> laptopCollections;
    private List<String> laptops;

    public ExpandableListAdapter(Activity context, Map<String, DriverPaymentInfo> laptopCollections) {
        this.context = context;
        this.laptopCollections = laptopCollections;
        this.laptops = new ArrayList<String>(laptopCollections.keySet());
    }

    public Object getChild(int groupPosition, int childPosition) {
        return laptopCollections.get(laptops.get(groupPosition));
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final DriverPaymentInfo paymentInfo = (DriverPaymentInfo) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_item, null);
        }

        TextView netEarningTv = (TextView) convertView.findViewById(R.id.driver_net_earning_tv);
        TextView cashCollectedTv = (TextView) convertView.findViewById(R.id.driver_cash_collected_tv);
        TextView paymentByAvkTv = (TextView) convertView.findViewById(R.id.driver_payment_avk_tv);
        TextView pendingAmountTv = (TextView) convertView.findViewById(R.id.driver_pending_amount_tv);


        netEarningTv.setText("Rs. " + paymentInfo.getNetEarnings());
        cashCollectedTv.setText("Rs. " + paymentInfo.getCashCollected());
        paymentByAvkTv.setText("Rs. " + paymentInfo.getPaymentByAkv());
        pendingAmountTv.setText("Rs. " + paymentInfo.getPendingAmount());
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return 1;// laptopCollections.get(laptops.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return laptops.get(groupPosition);
    }

    public int getGroupCount() {
        return laptops.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String laptopName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item,
                    null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.laptop);
        item.setText(laptopName);
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
