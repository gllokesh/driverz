package com.example.gllokesh.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gllokesh.myapplication.model.DriverInfo;
import com.example.gllokesh.myapplication.model.DriverPaymentInfo;
import com.example.gllokesh.myapplication.model.PaymentInfo;
import com.example.gllokesh.myapplication.model.TariffInfo;
import com.example.gllokesh.myapplication.model.TransactionInfo;
import com.example.gllokesh.myapplication.utils.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gllokesh on 10/28/17.
 */

public class DriverInfoActivity extends AppCompatActivity {
    private String driverName;
    private ExpandableListView expListView;
    private Map<String, ArrayList<PaymentInfo>> paymentInfoMap = new HashMap<String, ArrayList<PaymentInfo>>();
    private Map<String, TransactionInfo> transactionInfoMap;
    private Map<Double, ArrayList<TariffInfo>> tariffMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_info);
        Bundle bundle = getIntent().getExtras();
        expListView = (ExpandableListView) findViewById(R.id.driver_info_list);
        if (bundle != null) {
            if (bundle.containsKey("driver_name")) {
                driverName = bundle.getString("driver_name");
            }
        }
        transactionInfoMap = new HashMap<String, TransactionInfo>();
        tariffMap = new HashMap<Double, ArrayList<TariffInfo>>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView headerTitleTv = (TextView) toolbar.findViewById(R.id.activity_title);
        headerTitleTv.setText(driverName + "'s Details info.");
        /*Utility.showProgressDialog(this);
        loadDriverPaymentInfo();*/
        loadTariffDataFromDb();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void loadDriverInfo(String name) {
        Query q = DriverApplication.getDriverDatabaseRef();//.orderByChild("drivername").startAt(name).endAt(name);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Utility.hideProgressDialog();
                if (dataSnapshot.exists()) {
                    Map<String, List<DriverInfo>> driverMap = new HashMap<String, List<DriverInfo>>();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        GenericTypeIndicator<ArrayList<DriverInfo>> t = new GenericTypeIndicator<ArrayList<DriverInfo>>() {
                        };

                        List<DriverInfo> driverInfo = postSnapshot.getValue(t);
                        for (DriverInfo info : driverInfo) {
                            Log.e("key", "==========>" + info.getDrivername());
                            if (driverName.equalsIgnoreCase(info.getDrivername())) {
                                if (driverMap.containsKey(info.getDate())) {
                                    List<DriverInfo> driverInfos = driverMap.get(info.getDate());
                                    driverInfos.add(info);
                                    driverMap.put(info.getDate(), driverInfos);
                                } else {
                                    List<DriverInfo> driverInfos = new ArrayList<DriverInfo>();
                                    driverInfos.add(info);
                                    driverMap.put(info.getDate(), driverInfos);
                                }
                            }
                        }
                    }
                    Log.e("key", "==========>" + driverMap.size());

                    calculateDriverInfoByDate(driverMap);
                } else {
                    Utility.hideProgressDialog();
                    Toast.makeText(DriverInfoActivity.this, "Information is not available. Please contact admin for detail information.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utility.hideProgressDialog();

            }
        });
    }


    private void calculateDriverInfoByDate(Map<String, List<DriverInfo>> driverMap) {
        Map<String, DriverPaymentInfo> leaderBoardMap = new HashMap<String, DriverPaymentInfo>();
        for (Map.Entry<String, List<DriverInfo>> entry : driverMap.entrySet()) {
            List<DriverInfo> drivers = entry.getValue();
            String tariffId = "0";
            double totalKms = 0.0;
            double cashCollected = 0;
            double toll = 0;

            for (DriverInfo driver : drivers) {
                if (!TextUtils.isEmpty(tariffId)) {
                    tariffId = transactionInfoMap.get(driver.getDate()).getTariffId();
                }
                cashCollected += Double.parseDouble(driver.getCash_collected_by_driver_Raw());
                toll += Double.parseDouble(driver.getToll_ParkingRaw());
                totalKms = totalKms + Double.parseDouble(driver.getActual_Kms_Raw());
                Log.e("key", "==========>" + driver.getDrivername());
            }


            double totalAmount = calculateTotalAmount(totalKms,Double.parseDouble(tariffId));
            DriverPaymentInfo paymentInfo = new DriverPaymentInfo();
            paymentInfo.setCashCollected(Math.abs(cashCollected) + "");
            paymentInfo.setToll(toll + "");
            paymentInfo.setNetEarnings(totalAmount + "");
            for (PaymentInfo info : paymentInfoMap.get(entry.getKey())) {
                if (driverName.equalsIgnoreCase(info.getDriverName())) {
                    paymentInfo.setPaymentByAkv((Double.parseDouble(paymentInfo.getPaymentByAkv()) + Double.parseDouble(info.getAmount())) + "");

                }
            }
            paymentInfo.setPendingAmount("Rs." + String.valueOf(totalAmount - Double.parseDouble(paymentInfo.getPaymentByAkv()) - Math.abs(cashCollected)));
            leaderBoardMap.put(entry.getKey(), paymentInfo);

        }
        final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(
                this, leaderBoardMap);
        expListView.setAdapter(expListAdapter);
        for (int i = 0; i < expListAdapter.getGroupCount(); i++) {
            expListView.expandGroup(i);
        }

    }

    private Double calculateTotalAmount(double totalKms, Double tariffId) {

        if (tariffMap.containsKey(tariffId)) {
            ArrayList<TariffInfo> infoList = tariffMap.get(tariffId);
            for (TariffInfo tariffInfo : infoList) {
                double min = Double.parseDouble(tariffInfo.getMinKms());
                double max = Double.parseDouble(tariffInfo.getMaxKms());
                if (totalKms >= min && totalKms <= max) {
                    return Double.parseDouble(tariffInfo.getAmount());
                }
            }
        }
        return 0.0;
    }

    private void loadTariffDataFromDb() {
        try {
            Utility.showProgressDialog(DriverInfoActivity.this);
            DriverApplication.getTarrifDatabaseRef().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    tariffMap = new HashMap<Double, ArrayList<TariffInfo>>();
                    while (iterable.iterator().hasNext()) {
                        DataSnapshot dataSnap = iterable.iterator().next();
                        TariffInfo tariffInfo = (TariffInfo) dataSnap.getValue(TariffInfo.class);
                        if (tariffInfo != null) {
                            Double tariffId = Double.parseDouble(tariffInfo.getTariffId());
                            if (tariffMap.containsKey(tariffId)) {
                                ArrayList<TariffInfo> list = tariffMap.get(tariffId);
                                list.add(tariffInfo);
                                tariffMap.put(tariffId, list);
                            } else {
                                ArrayList<TariffInfo> list = new ArrayList<TariffInfo>();
                                list.add(tariffInfo);
                                tariffMap.put(tariffId, list);
                            }
                        }
                    }
                    loadTransactionDataFromDb();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(DriverInfoActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void loadTransactionDataFromDb() {
        try {
            DriverApplication.getTransactionDatabaseRef().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Utility.hideProgressDialog();
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    while (iterable.iterator().hasNext()) {
                        DataSnapshot dataSnap = iterable.iterator().next();
                        TransactionInfo transactionInfo = (TransactionInfo) dataSnap.getValue(TransactionInfo.class);
                        if (transactionInfo != null) {
                            transactionInfo.setTariffInfoList(tariffMap.get(Double.parseDouble(transactionInfo.getTariffId())));
                            transactionInfoMap.put(transactionInfo.getDate(), transactionInfo);
                        }

                    }
                    DriverApplication.getTransactionDatabaseRef().removeEventListener(this);
                    loadDriverPaymentInfo();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Utility.hideProgressDialog();
                    Toast.makeText(DriverInfoActivity.this, "The read failed: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } catch (Exception e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }

    private void loadDriverPaymentInfo() {
        Utility.showProgressDialog(DriverInfoActivity.this);
        Query query = DriverApplication.getPaymentDatabaseRef();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    GenericTypeIndicator<HashMap<String, ArrayList<PaymentInfo>>> t = new GenericTypeIndicator<HashMap<String, ArrayList<PaymentInfo>>>() {
                    };
                    paymentInfoMap = dataSnapshot.getValue(t);
                    loadDriverInfo(driverName);
                } else {
                    Utility.hideProgressDialog();
                    Toast.makeText(DriverInfoActivity.this, "Information is not available. Please contact admin for detail information.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utility.hideProgressDialog();

            }
        });
    }

}
