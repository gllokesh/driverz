package com.example.gllokesh.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gllokesh.myapplication.model.DriverInfo;
import com.example.gllokesh.myapplication.model.KeyInfo;
import com.example.gllokesh.myapplication.model.PaymentInfo;
import com.example.gllokesh.myapplication.model.RolesInfo;
import com.example.gllokesh.myapplication.model.SpreadsheetKeys;
import com.example.gllokesh.myapplication.model.TariffInfo;
import com.example.gllokesh.myapplication.model.TransactionInfo;
import com.example.gllokesh.myapplication.model.VehicleInfo;
import com.example.gllokesh.myapplication.utils.Utility;
import com.google.firebase.database.DatabaseReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminActivity extends AppCompatActivity implements View.OnClickListener {


    private SpreadsheetKeys mSpreadsheetKeys;
    private List<TransactionInfo> transactionInfoList;
    private int currentTrasaction;
    private Map<String, ArrayList<DriverInfo>> driversListMap = new HashMap<String, ArrayList<DriverInfo>>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView headerTitleTv = (TextView) toolbar.findViewById(R.id.activity_title);
        headerTitleTv.setText("Admin Panel");

        Button updateRolesBtn = (Button) findViewById(R.id.update_roles_btn);
        Button updateTransactionsBtn = (Button) findViewById(R.id.update_transaction_btn);
        Button updateVehicleBtn = (Button) findViewById(R.id.update_vehicle_redg_btn);
        Button updateTariffBtn = (Button) findViewById(R.id.update_tariff_btn);
        RelativeLayout leaderLayout = (RelativeLayout) findViewById(R.id.leader_board_layout);
        Button leaderBoardBtn = (Button) findViewById(R.id.leader_board_btn);
        ImageView logOutBtn = (ImageView)findViewById(R.id.logout_btn);
        logOutBtn.setVisibility(View.VISIBLE);
        logOutBtn.setOnClickListener(this);
        leaderBoardBtn.setOnClickListener(this);
        leaderLayout.setOnClickListener(this);
        updateRolesBtn.setOnClickListener(this);
        updateTransactionsBtn.setOnClickListener(this);
        updateVehicleBtn.setOnClickListener(this);
        updateTariffBtn.setOnClickListener(this);

        Utility.showProgressDialog(this);
        downloadGoogleSheetKeys();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.update_transaction_btn:
                Utility.showProgressDialog(this);
                downloadTransactionsInfo();
                break;
            case R.id.update_roles_btn:
                Utility.showProgressDialog(this);
                downloadRolesInfoFromGoogleSheet();
                break;
            case R.id.update_vehicle_redg_btn:
                Utility.showProgressDialog(this);
                downloadVehicleInfoDataFromGoogleSheet(mSpreadsheetKeys.getVehicleSheetInfo().getKey(), true);
                break;
            case R.id.update_tariff_btn:
                Utility.showProgressDialog(this);
                downloadTariffInfoFromGoogleSheet();
                break;
            case R.id.leader_board_layout:
                Intent intent = new Intent(this, LeaderBoardActivity.class);
                intent.putExtra("from_driver", false);
                startActivity(intent);
                break;
            case R.id.leader_board_btn:
                Intent intent1 = new Intent(this, LeaderBoardActivity.class);
                intent1.putExtra("from_driver", false);
                startActivity(intent1);
                break;
            case R.id.logout_btn:
                Intent logoutIntent = new Intent(this, MainActivity.class);
                startActivity(logoutIntent);
                finish();
                break;
        }

    }

    private void downloadTariffInfoFromGoogleSheet() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseTarrifData(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key=14ENSdLcqk5s0BmlKdKSLAEI3g9yIaTXatML2OnIVCAg&gid=2000198270");
    }

    private void parseTarrifData(JSONObject jsonObject) {
        DatabaseReference roleDatabaseReference = DriverApplication.getTarrifDatabaseRef();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            Log.e("rows Tariff info" , rows.toString());

            for (int r = 1; r < rows.length(); ++r) {
                TariffInfo tariffInfo = new TariffInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                Log.e("Tariff info" , columns.toString());
                tariffInfo.setMinKms(columns.getJSONObject(0).getString("v"));
                tariffInfo.setMaxKms(columns.getJSONObject(1).getString("v"));
                tariffInfo.setAmount(columns.getJSONObject(2).getString("v"));
                tariffInfo.setTariffId(columns.getJSONObject(3).getString("v"));
                tariffInfo.setRoles(columns.getJSONObject(4).getString("v"));
                roleDatabaseReference.child(tariffInfo.getTariffId().replace(".","P") + " " + tariffInfo.getMinKms().replace(".","P") + " " + tariffInfo.getMaxKms().replace(".","P")).setValue(tariffInfo);
            }
            Utility.hideProgressDialog();
            Toast.makeText(this, "Tariff Database Updated successfully.", Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void downloadTransactionsInfo() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseTransactionData(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key=14ENSdLcqk5s0BmlKdKSLAEI3g9yIaTXatML2OnIVCAg&gid=150769480");
    }

    private void parseTransactionData(JSONObject jsonObject) {
        DatabaseReference roleDatabaseReference = DriverApplication.getTransactionDatabaseRef();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            transactionInfoList = new ArrayList<TransactionInfo>();
            for (int r = 0; r < rows.length(); ++r) {
                TransactionInfo transactionInfo = new TransactionInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                transactionInfo.setDate(columns.getJSONObject(0).getString("v"));
                transactionInfo.setRawCmsCode(columns.getJSONObject(1).getString("v"));
                transactionInfo.setTariffId(columns.getJSONObject(2).getString("v"));
                Log.e("Date", transactionInfo.getDate().replace("\\", ""));
                transactionInfoList.add(transactionInfo);
                roleDatabaseReference.child(transactionInfo.getDate().replace("\\", "")).setValue(transactionInfo);
            }
            downloadDriverDetailsFromGoogleSheet(transactionInfoList.get(0), 0);
            Toast.makeText(this, "Transactions Database Updated  successfully.", Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void downloadDriverDetailsFromGoogleSheet(TransactionInfo transactionInfo, final int current) {
        this.currentTrasaction = current;
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                updateOrCreatedDriverInfoDatabase(object, currentTrasaction);
            }
        }).execute("https://spreadsheets.google.com/tq?key=" + transactionInfo.getRawCmsCode());

    }

    private void downloadGoogleSheetKeys() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseGoogleSheetKeys(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key=14ENSdLcqk5s0BmlKdKSLAEI3g9yIaTXatML2OnIVCAg&gid=1021655831");
    }


    private void parseGoogleSheetKeys(JSONObject jsonObject) {
        Utility.hideProgressDialog();

        try {
            JSONArray rows = jsonObject.getJSONArray("rows");

            JSONObject rolesJson = rows.getJSONObject(1);
            KeyInfo rolesKeyInfo = new KeyInfo();
            JSONArray rolesColumns = rolesJson.getJSONArray("c");
            rolesKeyInfo.setDocumentName(rolesColumns.getJSONObject(0).getString("v"));
            rolesKeyInfo.setKey(rolesColumns.getJSONObject(1).getString("v"));
            rolesKeyInfo.setDate(rolesColumns.getJSONObject(2).getString("v"));


            JSONObject vehicleJson = rows.getJSONObject(2);
            KeyInfo vehicleKeyInfo = new KeyInfo();
            JSONArray vehicleColumns = vehicleJson.getJSONArray("c");
            vehicleKeyInfo.setDocumentName(vehicleColumns.getJSONObject(0).getString("v"));
            vehicleKeyInfo.setKey(vehicleColumns.getJSONObject(1).getString("v"));
            vehicleKeyInfo.setDate(vehicleColumns.getJSONObject(2).getString("v"));


            JSONObject tariffJson = rows.getJSONObject(3);
            KeyInfo tariffKeyInfo = new KeyInfo();
            JSONArray tariffColumns = tariffJson.getJSONArray("c");
            tariffKeyInfo.setDocumentName(tariffColumns.getJSONObject(0).getString("v"));
            tariffKeyInfo.setKey(tariffColumns.getJSONObject(1).getString("v"));
            tariffKeyInfo.setDate(tariffColumns.getJSONObject(2).getString("v"));


            mSpreadsheetKeys = new SpreadsheetKeys();
            mSpreadsheetKeys.setRoleSheetInfo(rolesKeyInfo);
            mSpreadsheetKeys.setTariffSheetInfo(tariffKeyInfo);
            mSpreadsheetKeys.setVehicleSheetInfo(vehicleKeyInfo);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void downloadRolesInfoFromGoogleSheet() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseAndUpdatedRolesInfo(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key=" + mSpreadsheetKeys.getRoleSheetInfo().getKey());
    }

    private void parseAndUpdatedRolesInfo(JSONObject jsonObject) {
        DatabaseReference roleDatabaseReference = DriverApplication.getRoleDatabaseRef();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            List<RolesInfo> rolesInfos = new ArrayList<RolesInfo>();
            for (int r = 0; r < rows.length(); ++r) {
                RolesInfo rolesInfo = new RolesInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                rolesInfo.setName(columns.getJSONObject(0).getString("v"));
                rolesInfo.setRole(columns.getJSONObject(1).getString("v"));
                rolesInfo.setId(columns.getJSONObject(2).getString("v"));
                rolesInfo.setPassword(columns.getJSONObject(3).getString("v"));
                rolesInfos.add(rolesInfo);
                roleDatabaseReference.child(rolesInfo.getName()).setValue(rolesInfo);
            }
            Utility.hideProgressDialog();
            Toast.makeText(this, "Roles Database Updated  successfully.", Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void downloadVehicleInfoDataFromGoogleSheet(String googleSheetKey, boolean isUpdate) {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                updateOrCreateVehicleInfoDatabase(object, true);
            }
        }).execute("https://spreadsheets.google.com/tq?key=" + googleSheetKey);
    }

    private void updateOrCreateVehicleInfoDatabase(JSONObject jsonObject, boolean isUpdate) {
        Log.e("JSON", jsonObject.toString());
        List<VehicleInfo> vehicleInfos = new ArrayList<VehicleInfo>();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");

            for (int r = 0; r < rows.length(); ++r) {
                VehicleInfo vehicleInfo = new VehicleInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                vehicleInfo.setVehicleNumber(columns.getJSONObject(0).getString("v"));
                vehicleInfo.setVehicleType(columns.getJSONObject(1).getString("v"));
                vehicleInfo.setVehicleName(columns.getJSONObject(2).getString("v"));
                vehicleInfo.setVehicleOwner(columns.getJSONObject(3).getString("v"));
                vehicleInfo.setVehicleOwnerNumber(columns.getJSONObject(4).getString("v"));
                vehicleInfo.setVehicleOwnerAddress(columns.getJSONObject(5).getString("v"));
                vehicleInfo.setVehicleOwnerShare(columns.getJSONObject(6).getString("v"));
                vehicleInfo.setVehicleServiceShare(columns.getJSONObject(7).getString("v"));
                vehicleInfo.setVehicleDriverShare(columns.getJSONObject(8).getString("v"));
                vehicleInfos.add(vehicleInfo);
                DriverApplication.getVehicleDatabaseRef().child(vehicleInfo.getVehicleNumber()).setValue(vehicleInfo);

            }
            Utility.hideProgressDialog();
            Toast.makeText(this, "Vehicle Database Updated  successfully.", Toast.LENGTH_SHORT).show();


        } catch (JSONException e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


    private void updateOrCreatedDriverInfoDatabase(JSONObject jsonObject, int currentTrasaction) {
        Log.e("JSON", jsonObject.toString());
        List<DriverInfo> driverInfos = new ArrayList<DriverInfo>();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");
            Log.e("Driver rows", rows.toString());

            for (int r = 0; r < rows.length(); ++r) {
                Log.e("Driver r", r + " ===");

                DriverInfo driverInfo = new DriverInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                driverInfo.setDate(Utility.getString(columns, 0));
                driverInfo.setCarnumber(Utility.getString(columns, 1));
                driverInfo.setDrivernumber(Utility.getString(columns, 2));
                driverInfo.setDrivername(Utility.getString(columns, 3));
                driverInfo.setCRN(Utility.getString(columns, 4));
                driverInfo.setCompletionStatus(Utility.getString(columns, 5));
                driverInfo.setCustomerBillRaw(Utility.getString(columns, 6));
                driverInfo.setPaid_by_ola_money_Raw(Utility.getString(columns, 7));
                driverInfo.setOperatorBillRaw(Utility.getString(columns, 8));
                driverInfo.setPeakPricingRaw(Utility.getString(columns, 9));
                driverInfo.setRideEarningsRaw(Utility.getString(columns, 10));

                driverInfo.setTDSRaw(Utility.getString(columns, 11));
                driverInfo.setToll_ParkingRaw(Utility.getString(columns, 12));
                driverInfo.setCash_collected_by_driver_Raw(Utility.getString(columns, 13));
                driverInfo.setOla_to_Pay(Utility.getString(columns, 14));
                driverInfo.setCategory(Utility.getString(columns, 15));
                driverInfo.setPick_up_time(Utility.getString(columns, 16));
                driverInfo.setActual_Kms_Raw(Utility.getString(columns, 17));
                driverInfo.setTrip_Time_Raw(Utility.getString(columns, 18));
                driverInfo.setFare_Raw(Utility.getString(columns, 19));
                driverInfo.setShare_OSNs(Utility.getString(columns, 20));
                driverInfo.setNumber_of_share_OSNs(Utility.getString(columns, 21));
                driverInfo.setBookings_Completed_Raw(Utility.getString(columns, 22));
                driverInfos.add(driverInfo);
                if (driversListMap.containsKey(driverInfo.getDate())) {
                    ArrayList<DriverInfo> driverInfoList = driversListMap.get(driverInfo.getDate());
                    driverInfoList.add(driverInfo);
                    driversListMap.put(driverInfo.getDate(), driverInfoList);
                } else {
                    ArrayList<DriverInfo> driverInfoList = new ArrayList<DriverInfo>();
                    driverInfoList.add(driverInfo);
                    driversListMap.put(driverInfo.getDate(), driverInfoList);
                }

            }

            for (Map.Entry<String, ArrayList<DriverInfo>> entry : driversListMap.entrySet()) {
                String key = entry.getKey();
                Log.e("key =====", key);
                ArrayList<DriverInfo> driversList = entry.getValue();
                DriverApplication.getDriverDatabaseRef().child(key).setValue(driversList);
            }
            currentTrasaction = currentTrasaction + 1;
            if (currentTrasaction < transactionInfoList.size()) {
                downloadDriverDetailsFromGoogleSheet(transactionInfoList.get(currentTrasaction), currentTrasaction);
            } else {
                downloadPaymentInfoFromGoogleSheet();
            }
        } catch (JSONException e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }

    private void downloadPaymentInfoFromGoogleSheet() {
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                parseAndUpdatedPaymentsInfo(object);
            }
        }).execute("https://spreadsheets.google.com/tq?key=14ENSdLcqk5s0BmlKdKSLAEI3g9yIaTXatML2OnIVCAg&gid=1110589914");
    }

    private void parseAndUpdatedPaymentsInfo(JSONObject jsonObject) {
        Log.e("Payment info", jsonObject.toString());
        Map<String, ArrayList<PaymentInfo>> paymentMap = new HashMap<String, ArrayList<PaymentInfo>>();
        try {
            JSONArray rows = jsonObject.getJSONArray("rows");

            for (int r = 0; r < rows.length(); ++r) {
                PaymentInfo paymentInfo = new PaymentInfo();
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");
                paymentInfo.setDate(columns.getJSONObject(0).getString("v"));
                paymentInfo.setDriverName(columns.getJSONObject(1).getString("v"));
                paymentInfo.setAmount(columns.getJSONObject(2).getString("v"));

                if (paymentMap.containsKey(paymentInfo.getDate())) {
                    ArrayList<PaymentInfo> driverInfoList = paymentMap.get(paymentInfo.getDate());
                    driverInfoList.add(paymentInfo);
                    paymentMap.put(paymentInfo.getDate(), driverInfoList);
                } else {
                    ArrayList<PaymentInfo> driverInfoList = new ArrayList<PaymentInfo>();
                    driverInfoList.add(paymentInfo);
                    paymentMap.put(paymentInfo.getDate(), driverInfoList);
                }

                for (Map.Entry<String, ArrayList<PaymentInfo>> entry : paymentMap.entrySet()) {
                    String key = entry.getKey();
                    Log.e("key =====", key);
                    ArrayList<PaymentInfo> driversList = entry.getValue();
                    DriverApplication.getPaymentDatabaseRef().child(key).setValue(driversList);
                }

//                DriverApplication.getPaymentDatabaseRef().child(paymentInfo.getDate()).setValue(paymentInfo);
            }
            Utility.hideProgressDialog();
            Toast.makeText(this, "Vehicle Database Updated  successfully.", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Utility.hideProgressDialog();
            e.printStackTrace();
        }
    }


}
