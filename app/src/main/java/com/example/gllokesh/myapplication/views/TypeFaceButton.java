package com.example.gllokesh.myapplication.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.example.gllokesh.myapplication.R;
import com.example.gllokesh.myapplication.utils.Font;


public class TypeFaceButton extends Button {

    public TypeFaceButton(final Context context) {
        this(context, null);
    }

    public TypeFaceButton(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TypeFaceButton(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        if (this.isInEditMode()) {
            return;
        }

        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView);
        if (array != null) {
            final int value = array.getInteger(R.styleable.TypefaceTextView_customTypeface, 0);
            final Typeface typeface = Font.getTypefaceByValue(context, value);
            if (typeface != null) {
                setTypeface(typeface);
            }
            array.recycle();
        }
    }
}
