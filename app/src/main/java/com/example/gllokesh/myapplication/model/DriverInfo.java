package com.example.gllokesh.myapplication.model;

/**
 * Created by photonuser on 7/17/17.
 */

public class DriverInfo {

    private String Drivernumber;

    private String Bookings_Completed_Raw;

    private String Fare_Raw;

    private String TDSRaw;

    private String Trip_Time_Raw;

    private String Actual_Kms_Raw;

    private String Ola_to_Pay;

    private String Pick_up_time;

    private String Carnumber;

    private String Toll_ParkingRaw;

    private String Category;


    private String Paid_by_ola_money_Raw;

    private String CustomerBillRaw;

    private String Cash_collected_by_driver_Raw;

    private String Date;

    private String Share_OSNs;

    private String Number_of_share_OSNs;

    private String CompletionStatus;

    private String OperatorBillRaw;

    private String PeakPricingRaw;

    private String RideEarningsRaw;

    private String CRN;

    private String Drivername;

    private String tariffId;

    public String getDrivernumber() {
        return Drivernumber;
    }

    public void setDrivernumber(String drivernumber) {
        Drivernumber = drivernumber;
    }

    public String getBookings_Completed_Raw() {
        return Bookings_Completed_Raw;
    }

    public void setBookings_Completed_Raw(String bookings_Completed_Raw) {
        Bookings_Completed_Raw = bookings_Completed_Raw;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getFare_Raw() {
        return Fare_Raw;
    }

    public void setFare_Raw(String fare_Raw) {
        Fare_Raw = fare_Raw;
    }

    public String getTDSRaw() {
        return TDSRaw;
    }

    public void setTDSRaw(String TDSRaw) {
        this.TDSRaw = TDSRaw;
    }

    public String getTrip_Time_Raw() {
        return Trip_Time_Raw;
    }

    public void setTrip_Time_Raw(String trip_Time_Raw) {
        Trip_Time_Raw = trip_Time_Raw;
    }

    public String getActual_Kms_Raw() {
        return Actual_Kms_Raw;
    }

    public void setActual_Kms_Raw(String actual_Kms_Raw) {
        Actual_Kms_Raw = actual_Kms_Raw;
    }

    public String getOla_to_Pay() {
        return Ola_to_Pay;
    }

    public void setOla_to_Pay(String ola_to_Pay) {
        Ola_to_Pay = ola_to_Pay;
    }

    public String getPick_up_time() {
        return Pick_up_time;
    }

    public void setPick_up_time(String pick_up_time) {
        Pick_up_time = pick_up_time;
    }

    public String getCarnumber() {
        return Carnumber;
    }

    public void setCarnumber(String carnumber) {
        Carnumber = carnumber;
    }

    public String getToll_ParkingRaw() {
        return Toll_ParkingRaw;
    }

    public void setToll_ParkingRaw(String toll_ParkingRaw) {
        Toll_ParkingRaw = toll_ParkingRaw;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }


    public String getPaid_by_ola_money_Raw() {
        return Paid_by_ola_money_Raw;
    }

    public void setPaid_by_ola_money_Raw(String paid_by_ola_money_Raw) {
        Paid_by_ola_money_Raw = paid_by_ola_money_Raw;
    }

    public String getCustomerBillRaw() {
        return CustomerBillRaw;
    }

    public void setCustomerBillRaw(String customerBillRaw) {
        CustomerBillRaw = customerBillRaw;
    }

    public String getCash_collected_by_driver_Raw() {
        return Cash_collected_by_driver_Raw;
    }

    public void setCash_collected_by_driver_Raw(String cash_collected_by_driver_Raw) {
        Cash_collected_by_driver_Raw = cash_collected_by_driver_Raw;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getShare_OSNs() {
        return Share_OSNs;
    }

    public void setShare_OSNs(String share_OSNs) {
        Share_OSNs = share_OSNs;
    }

    public String getNumber_of_share_OSNs() {
        return Number_of_share_OSNs;
    }

    public void setNumber_of_share_OSNs(String number_of_share_OSNs) {
        Number_of_share_OSNs = number_of_share_OSNs;
    }

    public String getCompletionStatus() {
        return CompletionStatus;
    }

    public void setCompletionStatus(String completionStatus) {
        CompletionStatus = completionStatus;
    }

    public String getOperatorBillRaw() {
        return OperatorBillRaw;
    }

    public void setOperatorBillRaw(String operatorBillRaw) {
        OperatorBillRaw = operatorBillRaw;
    }

    public String getPeakPricingRaw() {
        return PeakPricingRaw;
    }

    public void setPeakPricingRaw(String peakPricingRaw) {
        PeakPricingRaw = peakPricingRaw;
    }

    public String getRideEarningsRaw() {
        return RideEarningsRaw;
    }

    public void setRideEarningsRaw(String rideEarningsRaw) {
        RideEarningsRaw = rideEarningsRaw;
    }

    public String getCRN() {
        return CRN;
    }

    public void setCRN(String CRN) {
        this.CRN = CRN;
    }

    public String getDrivername() {
        return Drivername;
    }

    public void setDrivername(String drivername) {
        Drivername = drivername;
    }

    @Override
    public String toString() {
        return this.getDrivername() +"   " + this.getDrivernumber();
    }
}
