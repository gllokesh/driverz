package com.example.gllokesh.myapplication.model;

/**
 * Created by gllokesh on 10/8/17.
 */

public class PaymentInfo {
    private String date;
    private String driverName;
    private String amount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
