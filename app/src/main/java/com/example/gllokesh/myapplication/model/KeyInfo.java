package com.example.gllokesh.myapplication.model;

/**
 * Created by gllokesh on 8/29/17.
 */

public class KeyInfo {
    private String documentName;
    private String key;
    private String date;

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
