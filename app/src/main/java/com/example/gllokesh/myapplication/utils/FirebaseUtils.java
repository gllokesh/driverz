package com.example.gllokesh.myapplication.utils;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by gllokesh on 8/29/17.
 */

public class FirebaseUtils {


    public static DatabaseReference getRolesDatabaseReference(Application context) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyAU5_gv5t7B7pCjv1VVBJEJukv34tLukhY")
                .setApplicationId("1:773871353235:android:3215865ca16fc7bd")
                .setDatabaseUrl("https://roles-d6855.firebaseio.com/")
                .build();
        FirebaseApp rolesApp = FirebaseApp.initializeApp(context, options, "Roles App");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(rolesApp);
        return firebaseDatabase.getReference();
    }


    public static DatabaseReference getVehicleRegdDatabaseReference(Application context) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyAkfdDMWga2uo0FhmgQx2osXgRMRArP-Ys")
                .setApplicationId("1:1023179497362:android:3215865ca16fc7bd")
                .setDatabaseUrl("https://vehicleinfo-96972.firebaseio.com/")
                .build();
        FirebaseApp vehicleApp = FirebaseApp.initializeApp(context, options, "Vehicle Regd");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(vehicleApp);
        return firebaseDatabase.getReference();
    }


    public static DatabaseReference getTariffDatabaseReference(Application context) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyCfREYqSoPjAvs_oP0OLHwfYvOyj9VcEYo")
                .setApplicationId("1:1015287760661:android:3215865ca16fc7bd")
                .setDatabaseUrl("https://tariff-3f641.firebaseio.com/")
                .build();
        FirebaseApp tariffApp = FirebaseApp.initializeApp(context, options, "Tariff");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(tariffApp);
        return firebaseDatabase.getReference();
    }


    public static DatabaseReference getRawCrnsDatabaseReference(Application context) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyBBMbYFK3D_TZAX3Cct7NTEl2YAbaQoU2Y")
                .setApplicationId("1:353361604052:android:3215865ca16fc7bd")
                .setDatabaseUrl("https://rawcrns.firebaseio.com/")
                .build();
        FirebaseApp rawCrnsApp = FirebaseApp.initializeApp(context, options, "RawCrns");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(rawCrnsApp);
        return firebaseDatabase.getReference();
    }


    public static DatabaseReference getTransactionDatabaseReference(Application context) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyDBkma3MlDGFrylo7kX0M-pB8qxgzZFH8I")
                .setApplicationId("1:380819104923:android:3215865ca16fc7bd")
                .setDatabaseUrl("https://transaction-d4852.firebaseio.com/")
                .build();
        FirebaseApp rawCrnsApp = FirebaseApp.initializeApp(context, options, "Transaction");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(rawCrnsApp);
        return firebaseDatabase.getReference();
    }


    public static DatabaseReference getPaymentsDatabaseReference(Application context) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyB6dcUr2K9XTbsavibL5Kea5qpdje-y8JE")
                .setApplicationId("1:681014983056:android:3215865ca16fc7bd")
                .setDatabaseUrl("https://payments-e47ae.firebaseio.com/")
                .build();
        FirebaseApp rawCrnsApp = FirebaseApp.initializeApp(context, options, "Payments");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(rawCrnsApp);
        return firebaseDatabase.getReference();
    }

}
